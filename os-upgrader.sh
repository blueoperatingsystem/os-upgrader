echo You are running BlueOS $(lsb_release -rs)
echo Checking for updates...
if grep -Fxq "big-patch1" /usr/share/blueos-release
then
  echo No full operating system upgrades have been found. If you want to upgrade your system please use Discover.
  sleep 5
else
  echo "Upgrading Wallpapers..."
  cd /usr/share/backgrounds/blueos
  sudo git pull
  sleep 1
  echo Upgrading .bashrc...
  wget -O ~/.bashrc https://gitlab.com/blueoperatingsystem/custom/-/raw/master/.bashrc
  source ~/.bashrc
  echo Adding .desktop file for upgrader
  wget -O "/home/$USER/Desktop/Upgrade BlueOS.desktop" https://gitlab.com/blueoperatingsystem/custom/-/raw/master/blueupgrader.desktop
  wget -O "/home/$USER/Schreibtisch/Upgrade BlueOS.desktop" https://gitlab.com/blueoperatingsystem/custom/-/raw/master/blueupgrader.desktop
  sudo wget -O "/etc/skel/Desktop/Upgrade BlueOS.desktop" https://gitlab.com/blueoperatingsystem/custom/-/raw/master/blueupgrader.desktop
  sudo wget -O "/usr/share/applications/Upgrade BlueOS.desktop" https://gitlab.com/blueoperatingsystem/custom/-/raw/master/blueupgrader.desktop
  gio set "/home/$USER/Desktop/Upgrade BlueOS.desktop" "metadata::trusted" yes
  gio set "/home/$USER/Schreibtisch/Upgrade BlueOS.desktop" "metadata::trusted" yes
  echo Installing BlueOS upgrader...
  sudo wget -O /usr/bin/blueupgrader https://gitlab.com/blueoperatingsystem/custom/-/raw/master/blueupgrader
  sudo chmod +x /usr/bin/blueupgrader
  echo Upgrading /etc/sudoers...
  sudo grep -q "Defaults pwfeedback" /etc/sudoers
  if [[ $? != 0 ]]; then
    echo "Defaults pwfeedback" | sudo tee -a /etc/sudoers
  fi
  echo Upgrading installed packages...
  sudo add-apt-repository ppa:alexlarsson/flatpak -y
  sudo add-apt-repository ppa:kubuntu-ppa/backports -y
  sudo apt update
  sudo apt install flatpak -y
  sudo apt update
  sudo apt dist-upgrade -y
  sudo apt autoremove -y
  sudo flatpak remote-delete flathub --force --system
  sudo flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
  flatpak update -y
  flatpak update -y
  echo Adding update daemon...
  sudo wget -O /usr/bin/blueos-update-daemon https://gitlab.com/blueoperatingsystem/custom/-/raw/master/blueos-update-daemon.py
  sudo chmod +x /usr/bin/blueos-update-daemon
  sudo wget -O /etc/skel/.config/autostart/blueos-update-daemon.desktop https://gitlab.com/blueoperatingsystem/custom/-/raw/master/blueos-update-daemon.desktop
  wget -O ~/.config/autostart/blueos-update-daemon.desktop https://gitlab.com/blueoperatingsystem/custom/-/raw/master/blueos-update-daemon.desktop
  echo Adding AppImageLauncher settings to control panel...
  sudo wget -O /usr/share/applications/appimagelaunchersettings.desktop https://raw.githubusercontent.com/TheAssassin/AppImageLauncher/master/resources/appimagelaunchersettings.desktop
  if  [ ! -f ~/.panelconfig ]; then
    echo Patching panel...
    wget -O /tmp/panel.conf.tar.bz2 https://gitlab.com/blueoperatingsystem/custom/-/raw/master/panel.conf.tar.bz2
    sudo wget -O /usr/share/panel.conf.tar.bz2 https://gitlab.com/blueoperatingsystem/custom/-/raw/master/panel.conf.tar.bz2
    xfpanel-switch load /tmp/panel.conf.tar.bz2
    rm /tmp/panel.conf.tar.bz2
    touch ~/.panelconfig
  else
    echo Panel already patched. Skipping...
  fi
  echo Installing Pidgin...
  if grep -Fxq "pidgin-patch1" /usr/share/blueos-release
  then
    echo Pidgin already installed. Skipping...
  else
    sudo apt install pidgin -y
  fi
  echo Adding online accounts setting...
  sudo apt install gnome-online-accounts -y
  echo "gnome-control-center online-accounts" | sudo tee /usr/bin/online-accounts > /dev/null
  sudo chmod +x /usr/bin/online-accounts
  sudo wget -O /usr/share/applications/gnome-online-accounts-panel.desktop https://gitlab.com/blueoperatingsystem/custom/-/raw/master/gnome-online-accounts-panel.desktop
  echo "Adding plymouth splash..."
  if  [ ! -f /usr/share/plymouth/themes/blueos-bootsplash/blueos-bootsplash.plymouth ]; then
    cd /usr/share/plymouth/themes
    sudo mkdir ./blueos-bootsplash
    cd blueos-bootsplash
    sudo git clone https://gitlab.com/blueoperatingsystem/bootsplash .
    sudo update-alternatives --install /usr/share/plymouth/themes/default.plymouth default.plymouth /usr/share/plymouth/themes/blueos-bootsplash 99
    sudo ln -sf /usr/share/plymouth/themes/blueos-bootsplash/blueos-bootsplash.plymouth /etc/alternatives/default.plymouth
    sudo update-initramfs -u
  else
    echo "Plymouth splash already added. Skipping..."
  fi
  if grep -Fxq "xchat-patch1" /usr/share/blueos-release
  then
    echo XChat already installed. Skipping...
  else
    sudo apt install xchat -y
  fi
  curl -SsL https://updates.koyu.space/blueos/latest | sudo tee /usr/share/blueos-release
  echo "Running post-install hooks if any defined"
  if [ ! -f /etc/blueos-postinstall ]; then
    echo "No post-install hooks defined. Skipping..." | sudo tee /etc/blueos-postinstall
  else
    /etc/blueos-postinstall
  fi
  echo Cleaning up...
  echo "deb http://ppa.launchpad.net/kubuntu-ppa/backports/ubuntu bionic main" | sudo tee /etc/apt/sources.list.d/kubuntu-ppa-ubuntu-backports-bionic.list > /dev/null
  echo "deb http://ppa.launchpad.net/alexlarsson/flatpak/ubuntu bionic main" | sudo tee /etc/apt/sources.list.d/alexlarsson-ubuntu-flatpak-bionic.list > /dev/null
  cd /etc/apt/sources.list.d/
  sudo rm *.save
  sudo apt clean
  echo
  echo You have to reboot after applying these updates
  echo Upgrades finished, closing window...
fi